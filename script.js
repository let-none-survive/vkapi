	$('#btn').on('click', loadFriend);

		function getUrl(method, params) {
			if (!method) throw new Error('Нет метода');
			params = params || {};
			 params['access_token'] =//here write your token id
			return 'https://api.vk.com/method/'+ method +'?' + $.param(params);
		}

			function sendRequest(method, params, callback) {
					$.ajax({
				url: getUrl(method, params),
				method: "GET",
				dataType: "JSONP",
				success: callback
			});
			}

		function loadFriend() {
			sendRequest('friends.search', {count: 60, fields: 'photo_100,online'}, function(data) {
				drawFriends(data.response);
			});
		}

		 function drawFriends(friends) {
		 	let h = '';

		 	for(let i = 1; i < friends.length; i++){
		 		let f = friends[i];
		 		let online = f.online ? 'Online' : 'Offline'

		 		h += '<li>'+ 
		 				'<a target="_blank" href="http://vk.com/id'+ f.uid +'">'
		 					+'<img src="'+ f.photo_100+'"/>'
		 					+'<div>'
		 						+'<h4>' +f.first_name + ' ' + f.last_name +'</h4>' 
		 						+'<p>' + online + '</p>'		 					
		 					+'</div>'
		 				+'</a>'
		 			+'</li>';
		 	}
		 	$('ul').html(h);
		 }
